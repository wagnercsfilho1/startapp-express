var socketioJwt = require('socketio-jwt');

module.exports = function(io) {

    io.use(socketioJwt.authorize({
        secret: 'secret',
        handshake: true
    }));

    io.on('connection', function (socket) {
        require('../app/config/sockets')(io, socket);
    });
}