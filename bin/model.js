var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    fs = require('fs'),
    config = require('./config');


module.exports = function(app){

    function capitaliseFirstLetter(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    // dynamically include routes (Controller)
    fs.readdirSync(config.app.root+'/app/models').forEach(function (file) {
        if(file.substr(-3) == '.js') {
            var model = require('../app/models/'+file);
            var schema = new Schema(model.attributes);
            var Class = capitaliseFirstLetter(file.split('.')[0]);

            for (var i in model.methods){
               schema.methods[i] = model.methods[i];
            };

            for (var i in model.pre){
                schema.pre(i, model.pre[i]);
            };

            this[Class] = (function Model(){
                return  mongoose.model(Class, schema);
            })();
        }
    });


}