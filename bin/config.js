var _ = require('lodash');
var database = require('../app/config/database');
var app = require('../app/config/app');

exports.database = database;

exports.app = _.merge(
    app,
        require('../app/config/environment/' + process.env.NODE_ENV + '.js') || {});