var express = require('express');
var _ = require('lodash');
var composable_middleware = require( 'composable-middleware' );
var routes = require('../app/routes');
var config = require('./config');

module.exports = function(app){

    //tratar rotas
    var filter = function(filters){
        if (filters !== undefined){
            if (filters.length > 0) {
                var output = null;
                output = composable_middleware();
                filters.forEach(function (value) {
                    output.use(require('../app/filters')[value]);
                });
                return output;
            }
        }

        return composable_middleware()
            .use(function(req, res, next){
                next();
            });

    }

    routes.forEach(function(route){

        if (route.group != undefined){

            var groupBefore = route.before == undefined ? [] : route.before;
            var group = route.group;
            var prefix = '';

            var router = express.Router();
            route.routes.forEach(function(r){


                var method = r.method.toLowerCase();
                var path = r.path.toLowerCase();
                var controller = typeof(r.controller) == 'function' ? r.controller : r.controller.toLowerCase();
                var routeBefore = r.before == undefined ? undefined : r.before;
                var before = _.union(routeBefore, groupBefore);

                if (route.prefix != undefined){
                    prefix = route.prefix;
                }else{
                    prefix = config.app.prefix;
                }

                if (typeof(controller) === 'function') {
                    router[method](path, filter(before), controller);
                } else {
                    var controller = controller.split('@');
                    router[method](path, filter(before), require('../app/controllers/' + controller[0])[controller[1]]);
                }

            });

            app.use(prefix+group, router);

        } else {

            var method = route.method.toLowerCase();
            var path = route.path.toLowerCase();
            var controller = typeof(route.controller) == 'function' ? route.controller : route.controller.toLowerCase();
            var before = route.before == undefined ? undefined : route.before;
            var prefix = '';
            if (route.prefix != undefined){
                prefix = route.prefix;
            }else{
                prefix = config.app.prefix;
            }

            if (typeof(controller) == 'function') {
                app[method](prefix+path, filter(before), controller);
            } else {
                var controller = controller.split('@');
                app[method](prefix+path, filter(before), require('../app/controllers/' + controller[0])[controller[1]]);
            }
        }

    });
}