module.exports = function(app){

    // error handlers

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            //if (err.name === 'UnauthorizedError') {
            //    res.send(401, 'invalid token...');
            //}else {
                if (err.status == 404){
                    res.render('notfound');
                }else {
                    res.status(err.status || 500);
                    res.render('error', {
                        message: err.message,
                        error: err
                    });
                }
            //}
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });

}