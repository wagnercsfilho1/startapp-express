// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var debug = require('debug')('startapp');
var express = require('express');
var mongoose = require('mongoose');
var config = require('./config');
var events = require('events');
var eventEmitter = new events.EventEmitter();


// Connect to database
mongoose.connect(config.database.mongo.uri, config.database.mongo.options);

// Setup server
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

require('./express')(app);
require('../app/config/middleware')(app);
require('./socket')(io);
require('./model')(app);
require('./routes')(app);

// 404 and error handlers
require('./notfoundhttp')(app);
require('./error-handle')(app);

// Start server
server.listen(config.app.port, function() {
    debug('Express server listening on port ' + config.app.port + ' in ' +app.get('env'));
});

module.exports = app;