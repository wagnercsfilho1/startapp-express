var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var compression = require('compression');
var session = require('express-session');
var flash    = require('connect-flash');
var config = require('./config');

module.exports = function(app){

    // view engine setup
    app.set('views', path.join(config.app.root, 'app/views'));
    app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(__dirname + '/public/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(multer({
        dest: './public/uploads/',
        rename: function (fieldname, filename) {
            return filename.replace(/\W+/g, '-').toLowerCase();
        }
    }));
    app.use(cookieParser());
    app.use(compression()); //use compression
    app.use(express.static(path.join(config.app.root, 'public')));
    // required for passport
    app.use(session(
        {
            secret: 'secret',
            saveUninitialized: true,
            resave: true
        })
    ); // session secret

    app.use(flash()); // use connect-flash for flash messages stored in session

    //CORS middleware
    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Access-Token');
        next();
    });




}