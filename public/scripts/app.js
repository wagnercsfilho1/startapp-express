'use strict';


    angular.module('app', [
        'ngAnimate',
        'ngCookies',
        'ngStorage',
        'ui.router',
        'ngResource',
        'ui.slimscroll',
        'ui.bootstrap',
        'angularFileUpload'
    ])

    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .otherwise('/app/dashboard');
        $stateProvider
            .state('app', {
                abstract: true,
                url: '/app',
                templateUrl: 'views/app.html',
                authenticate: true
            })
            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'views/app_dashboard.html',
                authenticate: true
            })

            .state('app.profile', {
                url: '/profile/:id',
                templateUrl: 'views/profile/index.html',
                controller: 'ProfileCtrl',
                authenticate: true
            })

            .state('app.discovery', {
                url: '/discovery',
                templateUrl: 'views/discovery.html',
                controller: 'DiscoveryCtrl',
                authenticate: true
            })

            .state('app.communities', {
                url: '/communities',
                templateUrl: 'views/communities.html',
                controller: 'CommunitiesCtrl',
                authenticate: true
            })

            .state('app.communities.chat', {
                url: '/:id/chat',
                templateUrl: 'views/chat.html',
                controller: 'ChatCtrl',
                authenticate: true
            })


            .state('signup', {
                url: '/signup',
                templateUrl: 'views/signup.html',
                controller: 'SignUpCtrl',
                authenticate: false

            })

            .state('signin', {
                url: '/signin',
                templateUrl: 'views/signin.html',
                controller: 'SignInCtrl',
                authenticate: false

            })
    })
        .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
            return {
                // Add authorization token to headers
                request: function (config) {
                    config.headers = config.headers || {};
                    if ($cookieStore.get('token')) {
                        config.headers.Authorization = $cookieStore.get('token');
                    }
                    return config || $q.when(config);
                }

                // Intercept 401s and redirect you to login
                /*responseError: function(response) {
                    if(response.status === 401) {
                        $location.path('/signin');
                        // remove any stale tokens
                        $cookieStore.remove('token');
                        return $q.reject(response);
                    }
                    else {
                        return $q.reject(response);
                    }
                }*/
            };
        })

        .run(function ($rootScope, $window, $cookieStore, $state, Auth) {

            if ($cookieStore.get('token')) {
                $window.socket = io('http://localhost', {
                    query: 'token=' + $cookieStore.get('token')
                });

                $window.socket.on("error", function (error) {
                    if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
                        // redirect user to login page perhaps?
                        console.log("User's token has expired");
                    }
                });
            }

            // Redirect to login if route requires auth and you're not logged in
            $rootScope.$on('$stateChangeStart', function (event, next) {
                    if (next.authenticate && !Auth.isLoggedIn()) {
                        event.preventDefault();
                        $state.go('signin');
                    }
            });
        })

        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
        });

