'use strict';

angular.module('app')

    .controller('AppController', function ($scope, $modal, $state,  Auth) {
        $scope.logout = function(){
            Auth.logout();
            $state.go('signin');
        };

        $scope.create = function(){

            $modal.open({
                templateUrl: 'views/create-modal.html',
                controller: 'CreateCtrl'
            });

        }
    })

    .controller('CreateCtrl', function($scope, $modalInstance, $upload, $state, Socket, Community){

        var files = null;

        $scope.onFileSelect = function($files) {
            files = $files;
        };

        $scope.create = function (community) {

            var file = files[0];
            $scope.upload = $upload.upload({
                url: '/api/community',
                method: 'POST',
                data: community,
                file: file // or list of files ($files) for html5 only
            }).progress(function(evt) {
                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                $state.go('app.communities.chat', {id: data._id});
            });

            //Community.save(community, function(data){
            //    $state.go('app.communities.chat', {id: data._id});
            //});
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    })

    .controller('SignInCtrl',  function($rootScope, $window, $scope, $cookieStore, $http, $state, Auth, User, Socket){

        $scope.login = function(user){

            Auth.login(user)
                .then(function(data){


                    $state.go('app.dashboard');
                }, function(err){
                    console.log(err);
                });

        }

        $scope.auth = function(){
            var myWindow = $window.open("/api/auth/facebook", "myWindow", "width=500, height=400");

            $window.auth = function(token){
                $cookieStore.put('token', token);
                Socket.auth();
                User.get(function(data){
                    $rootScope.currentUser = data;
                    $state.go('app.dashboard');
                });
            }
        }

    })

    .controller('SignUpCtrl',  function($rootScope, $window, $scope, $cookieStore, $http, $state, Auth, User){

        $scope.auth = function(){

            var myWindow = $window.open("/api/auth/facebook", "myWindow", "width=500, height=400");

            $window.auth = function(token){
                $cookieStore.put('token', token);
                User.get(function(data){
                    $rootScope.currentUser = data;
                    $state.go('app.dashboard');
                });
            }
        }

        $scope.signup = function(user) {

            Auth.createUser(user)
                .then(function(data){
                    $state.go('app.dashboard');
                }, function(err){
                    console.log(err);
                });

        }
    })

    .controller('DashCtrl',  function($scope){

    })

    .controller('DiscoveryCtrl',  function($scope){

    })

    .controller('CommunitiesCtrl',  function($scope, $http, $stateParams){
        $scope.communities = [];

        $http.get('/api/community').success(function(data){
            $scope.communities = data;
        });

    })

    .controller('ChatCtrl',  function($scope, $timeout, $rootScope, $window, $http, $stateParams){
        $scope.chats = [];
        $scope.community = [];

        $('#inputchat').focus();
        //$('#inputchat').focusout(function(){
        //    $('#inputchat').focus();
        //})

        $timeout(function() {
            $window.socket.emit('join',{room: $stateParams.id});
        }, 0);

        $http.get('/api/community/'+$stateParams.id+'').success(function(data){
            $scope.community = data;
        });

        $http.get('/api/community/'+$stateParams.id+'/chat').success(function(data){
            $scope.chats = data;
            $timeout(function() {
                document.getElementById("scrollchat").scrollTop = document.getElementById("scrollchat").scrollHeight;
            }, 300);
        });

        $window.socket.on('get:message', function(data){
            $scope.$apply(function(){
                $scope.chats.push(data);
                $timeout(function() {
                    document.getElementById("scrollchat").scrollTop = document.getElementById("scrollchat").scrollHeight;
                }, 0);
            });
        });

        $('#imagefile').on('change', function(e){

            var file = e.originalEvent.target.files[0],
                reader = new FileReader();

            reader.onload = function(evt){
                console.log(evt.target.result);
                socket.emit('user image', evt.target.result, e.target.files, {community: $stateParams.id}, function(){
                    console.log('ds');
                });
                $timeout(function(){
                    $scope.chats.push(
                        {
                            message: null,
                            //image: {
                            //    thumb: evt.target.result,
                            //    original: evt.target.result
                            //},
                            date: new Date(),
                            _user: {
                                _id: $rootScope.currentUser._id,
                                avatar: {thumb: $rootScope.currentUser.avatar.thumb}
                            },
                            issend: false
                    })
                },0);
            };

            reader.readAsDataURL(file);
        });


            $('#file').change(function(e) {
                var file = e.target.files[0];
                var stream = ss.createStream();
                var blobStream = ss.createBlobReadStream(file);
                var size = 0;

                blobStream.on('data', function(chunk) {
                    size += chunk.length;
                    console.log(Math.floor(size / file.size * 100) + '%');
                    // -> e.g. '42%'
                });

                // upload a file to the server.
                ss(socket).emit('file', stream, {file: file});
                ss.createBlobReadStream(file).pipe(stream);



            });


        $scope.send = function(message){
            var index = $scope.chats.push({
                message: message,
                date: new Date(),
                _user: {
                    _id: $rootScope.currentUser._id,
                    avatar: {thumb: $rootScope.currentUser.avatar.thumb}
                },
                issend: false
            });
            $window.socket.emit('post:message', { message: message, community: $stateParams.id }, function(data){
                $timeout(function() {
                    $scope.chats[index - 1].issend = true;
                });
            });

            $timeout(function() {
                document.getElementById("scrollchat").scrollTop = document.getElementById("scrollchat").scrollHeight;
            }, 0);
        }


    })

    .controller('ProfileCtrl',  function($scope, $http, Profile, $stateParams){

        Profile.get($stateParams.id)
            .success(function(data){
                $scope.profile = data;

                if (data.facebook.cover != undefined) {
                    $http.get(data.facebook.cover).success(function (data) {
                        $scope.profile.facebook.cover = data.cover.source;
                    });
                }

            });

    });


