angular.module('app')
    .factory('Auth', function Auth($state, $rootScope, $http, User, $cookieStore, Socket, $q) {

        $rootScope.currentUser = {};

        if ($cookieStore.get('token')) {
            User.get(function(data){
                 $rootScope.currentUser = data;
            });
        }

        return {
            login: function (user) {
                var deferred = $q.defer();

                $http.post('http://localhost:3000/api/login', user)
                    .success(function (data) {
                        $cookieStore.put('token', data.token);
                        User.get(function(data){
                            $rootScope.currentUser = data;
                        });
                        Socket.auth();
                        deferred.resolve(data);
                    }).error(function (err) {
                        deferred.reject(err);
                    });

                return deferred.promise;
            },

            logout: function () {
                $cookieStore.remove('token');
                $rootScope.currentUser = false;
            },

            createUser: function (user, callback) {
                var deferred = $q.defer();

                $http.post('http://localhost:3000/api/signup', user)
                    .success(function (data) {
                        $cookieStore.put('token', data.token);
                        User.get(function(data){
                            $rootScope.currentUser = data;
                        });
                        Socket.auth();
                        deferred.resolve(data);
                    }).error(function (err) {
                        deferred.reject(err);
                    });

                return deferred.promise;
            },

            getCurrentUser: function () {
                return $rootScope.currentUser;
            },

            getToken: function () {
                return $cookieStore.get('token');
            },

            isLoggedIn: function() {
                if ($cookieStore.get('token')) {
                    return true;
                }else{
                    return false;
                }
            }
        }
    })

    .factory('User', function ($state, $rootScope, $http, $cookieStore, $resource, $q) {
        return $resource('/api/user/:id/:controller', {
                id: '@_id'
            },
            {
                changePassword: {
                    method: 'PUT',
                    params: {
                        controller:'password'
                    }
                },
                get: {
                    method: 'GET',
                    params: {
                        id:'me'
                    }
                }
            });

    })

    .factory('Community', function ($state, $rootScope, $http, $cookieStore, $resource, $q) {
        return $resource('/api/community/:id', {
                id: '@_id'
            },
            {
                update: {
                    method: 'PUT',
                    params: {
                        id: '@_id'
                    }
                }
            });

    })

    .factory('Profile', function ($state, $rootScope, $http, $cookieStore, $resource, $q) {

        return {
            get: function(id){
                return $http.get('/api/user/'+id);
            }
        }

    })

    .factory('Socket', function($window, $cookieStore){

        return {
            auth: function(){
                $window.socket = io('http://localhost', {
                    query: 'token=' + $cookieStore.get('token')
                });

                $window.socket.on("error", function(error) {
                    if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
                        // redirect user to login page perhaps?
                        console.log("User's token has expired");
                    }
                });
            }
        }


    });