module.exports = {

    attributes: {
        name: String,
        description: String,
        image: String,
        _creator : { type: String, ref: 'User' },
        members     : [{ type: String, ref: 'User' }]
    }

}