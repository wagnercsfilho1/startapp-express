module.exports = {

    attributes: {
        _user: {type: String, ref: 'User'},
        _community: {type: String, ref: 'Community'},
        message: String,
        image: {
            original: String,
            thumb: String
        },
        created_at: {type: Date, default: Date.now},
        updated_at: {type: Date, default: Date.now}
    }
    /*,

    pre:{
        'save': function(next){
            now = new Date();
            this.updated_at = now;
            if ( !this.created_at ) {
                this.created_at = now;
            }
            next();
        }
    }*/

}