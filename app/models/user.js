var bcrypt = require('bcrypt-nodejs');

module.exports = {

    attributes: {
        name: String,
        email: { type: String, lowercase: true },
        username: { type: String, lowercase: true },
        password: String,
        avatar: {
            thumb: String,
            profile: String,
            original: String
        },
        gender: { type: String },
        hometown: { type: String },
        locale: { type: String },
        timezone: { type: String },
        work: { type: String },
        facebook: {
            id: String,
            token: String,
            email: String,
            name: String,
            cover: String
        },
        twitter: {
            id: String,
            token: String,
            displayName: String,
            username: String
        },
        google: {
            id: String,
            token: String,
            email: String,
            name: String
        }
    },

    methods: {
        generateHash: function(password) {
            return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        },
        validPassword: function(password) {
            return bcrypt.compareSync(password, this.password);
        }
    }
};