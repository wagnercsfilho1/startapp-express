module.exports = {

    me: function(req, res){

        res.json(req.user);

    },

    get: function(req, res){

        User.findById(req.params.id, function(err, user){
            if (err) res.send(404);

            res.json(user);
        });

    }

}