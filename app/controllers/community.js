module.exports = {

    index: function(req, res){
        Community.find(function(err, communities){
           if (err) res.send(404);

            res.json(communities);
        });
    },

    show: function(req, res){
        Community.findById(req.params.id, function(err, communities){
            if (err) res.send(404);

            res.json(communities);
        });
    },

    store: function(req, res){
        var image = req.files.file.name;
        req.body.image = '/uploads/'+image;
        var community = new Community(req.body);
        community.save(function(err, newcommunity){
            if (err) res.send(500);

            res.json(newcommunity);
        });
    },

    getchat: function(req, res){

        Chat.find({_community: req.params.id}).populate('_user _community').exec(function(err, chats){
            if (err) res.send(404);

            res.json(chats);
        });

    },

    setchat: function(req, res){

        Chat.create(req.body, function(err, newchat){
            if (err) res.send(500);

            res.json(newchat);
        });

    }


}