var jwt = require('jsonwebtoken');
var passport = require('passport');

module.exports = {

    login: function(req, res, next){
        passport.authenticate('local-login', function(err, user, info) {
            if (err) { return next(err); }
            if (!user) { return res.send(401) }

            var token = jwt.sign(user, 'secret', { expiresInMinutes: 60*5 });

            res.json({token: token});


        })(req, res, next);
    },

    create: function(req, res, next){
        passport.authenticate('local-signup', function(err, user, info) {
            if (err) { return next(err); }
            if (!user) { return res.send(405); }

            var token = jwt.sign(user, 'secret', { expiresInMinutes: 60*5 });

            res.json({token: token});


        })(req, res, next);
    },

    facebook: function(req, res, next){
        passport.authenticate('facebook', { display: 'popup', scope : ['email','public_profile','user_friends','user_groups','user_hometown','user_likes'] })(req, res, next);
    },

    facebook_callback: function(req, res, next){
        passport.authenticate('facebook', function(err, user, info) {
            if (err) { return next(err); }
            if (!user) { return res.send(404); }


            var token = jwt.sign(user, 'secret', { expiresInMinutes: 60*5 });

            res.render('callback', {token: token});


        })(req, res, next);
    },

    logout: function(req, res, next){
        req.logout();
        res.redirect('/');
    }

}