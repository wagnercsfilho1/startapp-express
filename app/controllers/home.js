module.exports = {

    index: function(req, res){
        res.render('home/index', {title: 'StartApp'});
    },

    edit: function(req, res){
        res.json({nome: 'Wagner'});
    },

    show: function(req, res){
        User.find(function(err, user){
            res.json(user);
        });
    }

}