module.exports = [

    { method: 'GET', prefix: '',  path: '/', controller: function(req, res){  res.render('index');  } },

    /* Auth */
    { method: 'POST', path: '/login', controller: 'auth@login'},
    { method: 'POST', path: '/signup', controller: 'auth@create'},
    { method: 'GET', path: '/logout', controller: 'auth@logout'},
    { method: 'GET',  path: '/auth/facebook', controller: 'auth@facebook' },
    { method: 'GET',  path: '/auth/facebook/callback', controller: 'auth@facebook_callback' },

    /* Users */
    { method: 'GET', path: '/user/me', controller: 'user@me', before: ['authToken']},
    { method: 'GET', path: '/user/:id', controller: 'user@get', before: ['authToken']},

    /* Communities */
    {
        group: '/community',
        before: ['authToken'],
        routes: [
            { method: 'GET', path: '/', controller: 'community@index'},
            { method: 'GET', path: '/:id', controller: 'community@show'},
            { method: 'POST', path: '/', controller: 'community@store' },
            { method: 'GET', path: '/:id/chat', controller: 'community@getchat'},
            { method: 'POST', path: '/:id/chat', controller: 'community@setchat'}
        ]
    },


    /* Dashboard */
    { method: 'GET', path: '/dashboard', controller: 'dashboard@index', before: ['authPassport']},


];
