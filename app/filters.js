//var jwt = require('express-jwt');
var jwt = require('jsonwebtoken');

module.exports = {

    authSession: function(req, res, next){
        if (req.session.user)
            return next();

        res.redirect('/login');
    },

    authToken: function(req, res, next){
        jwt.verify(req.headers.authorization, 'secret', function(err, decoded) {
           if (err){
               res.send(401);
           } else {
               req.user = decoded;
               next();
           }
        });
    },

    authPassport: function(req, res, next) {
        // if user is authenticated in the session, carry on
        if (req.isAuthenticated()) {
            next();
        }else {
            // if they aren't redirect them to the home page
            res.redirect('/');
        }
    }

}
