var ss = require('socket.io-stream');
var path = require('path');
var fs = require('fs');

function decodeBase64Image(dataString) {
    var matches = dataString.substring("data:audio/mp3;base64,".length+1),
        response = {};

    if (matches.length !== 3) {
        //return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches, 'base64');

    return response;
}

module.exports = function(io, socket){

    socket.on('join', function(data){
        socket.join(data.room);
    });



    socket.on('user image', function (msg, file, data, cb) {

        var name = file[0].name;
        var type = file[0].type;

        var imageBuffer = decodeBase64Image(msg);

        fs.writeFile('public/uploads/'+name, imageBuffer.data, 'binary',  function(err) {
            console.log(err);

            var chat = Chat();
            chat.message = null;
            chat._community = data.community;
            chat._user = socket.decoded_token._id;
            chat.image.original = '/uploads/'+name;
            chat.image.thumb = '/uploads/'+name;
            console.log(chat.image.original);
            chat.save(function(err, newchat){
                if (err) console.log(err);

                Chat.findById(newchat._id).populate('_user').exec(function(err, chats){
                    if (err) console.log(err);
                    cb('ok');
                    socket.broadcast.to(data.community).emit('get:message', chats);
                });

            });
        });

        //Received an image: broadcast to all
       // socket.broadcast.emit('user image', socket.nickname, msg);
    });


    ss(socket).on('file', function(stream, data) {
        var filename = path.basename(data.name);
        //console.log('');
        stream.pipe(fs.createWriteStream('teste.wmv'));
    });

    socket.on('post:message', function (data, cb) {
        var chat = Chat();
        chat.message = data.message;
        chat._community = data.community;
        chat._user = socket.decoded_token._id;
        chat.save(function(err, newchat){
            if (err) console.log(err);

            Chat.findById(newchat._id).populate('_user').exec(function(err, chats){
                if (err) console.log(err);
                cb('ok');
                socket.broadcast.to(data.community).emit('get:message', chats);
            });

        });
    });
}