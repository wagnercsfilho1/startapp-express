var path = require('path');

module.exports = {

    // Root path of server
    root: path.normalize(__dirname + '/../..'),

    // Server port
    port: process.env.PORT || 3000,

    prefix: '/api',

    facebook : {
        'clientID'      : '230370410351342', // your App ID
        'clientSecret'  : '892ee2fe524812ca181f67b9b20770f0', // your App Secret
        'callbackURL'   : 'http://localhost:3000/api/auth/facebook/callback'
    },

    twitter : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },

    google : {
        'clientID'      : 'your-secret-clientID-here',
        'clientSecret'  : 'your-client-secret-here',
        'callbackURL'   : 'http://localhost:8080/auth/google/callback'
    }

};